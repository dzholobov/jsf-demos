package jsfajaxdemo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class PersonBean implements Serializable {

	private List<Person> persons = Arrays.asList(
			new Person(0, "Иван Иванов", 25, "Москва"),
			new Person(1, "Андрей Петров", 25, "Москва"),
			new Person(2, "Ольга Сергеева", 25, "Астрахань"),
			new Person(3, "Сергей Сидоров", 25, "Астрахань"),
			new Person(4, "Олег Андреев", 25, "Астрахань"));

	private String searchString;
	private List<Person> foundPersons = null;

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String name) {
		this.searchString = name;
	}

	public List<Person> getFoundPersons() {
		return foundPersons;
	}

	public void setFoundPersons(List<Person> foundPersons) {
		this.foundPersons = foundPersons;
	}

	public String search() {
		foundPersons = new ArrayList<Person>();
		if (searchString == null || searchString.isEmpty())
			return "";
		for (Person p: persons) {
			if (p.getName().toLowerCase().contains(searchString.toLowerCase()))
				foundPersons.add(p);
		}
		return "";
	}

}
