package ru.edu.asu.jsfhello;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class HelloBean {

	private String name;
	private String response;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getResponse() {
		return response;
	}

	public String sayHello() {
		response = "Здравствуйте, " + name;
		return "";
	}
}
