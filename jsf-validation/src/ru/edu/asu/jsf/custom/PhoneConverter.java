package ru.edu.asu.jsf.custom;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("phone")
public class PhoneConverter implements Converter {

	public Object getAsObject(FacesContext ctx, UIComponent component, String stringValue) 
			throws ConverterException {
		if (stringValue == null)
			return null;
		if (stringValue.length() == 7) {
			int pos = stringValue.indexOf("-");
			if (pos == -1)
				throw new ConverterException();
			String newString = stringValue.substring(0, pos) + stringValue.substring(pos + 1);
			try {
				Integer result = Integer.valueOf(newString);
				return result;
			} catch (NumberFormatException e) {
				throw new ConverterException();
			}
		} else
			throw new ConverterException();
	}

	public String getAsString(FacesContext ctx, UIComponent component, Object objectValue) {
		if (objectValue == null)
			return null;
		Integer intValue;
		try {
			intValue = (Integer)objectValue;
		} catch (ClassCastException e) {
			throw new ConverterException();
		}
		String newString = intValue.toString();
		if (newString.length() != 6)
			throw new ConverterException();
		String result = newString.substring(0, 3) + "-" + newString.substring(3);
		return result;
	}

}
