package ru.edu.asu.jsf.manual;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class ManualValidatorBean {

	private String login;
	private String age;
	private int ageValue;
	private String percent;
	private double percentValue;
	private List<String> errorMessages;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
		try {
			this.ageValue = Integer.parseInt(age);
		} catch (NumberFormatException e) {};
	}
	public String getPercent() {
		return percent;
	}
	public void setPercent(String percent) {
		this.percent = percent;
		try {
			this.percentValue = Double.parseDouble(percent);
		} catch (NumberFormatException e) {};
	}
	
	public String enterValue() {
		errorMessages = new ArrayList<String>();
		if (login.equals(""))
			errorMessages.add("Укажите логин");
		if (ageValue < 16)
			errorMessages.add("Возраст должен быть больше 16");
		if ((percentValue < 0.01) || (percentValue > 0.99))
			errorMessages.add("Ставка должна быть от 0.01 до 0.99");
		if (errorMessages.size() > 0)
			return null;
		else 
			return "/result";
	}
	
	public String getErrors() {
		String errors = "";
		if (errorMessages != null)
			for (String error: errorMessages)
				errors += (error + "<br>");
		return errors;
	}
}
