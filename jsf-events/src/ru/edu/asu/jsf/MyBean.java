package ru.edu.asu.jsf;

import java.io.Serializable;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class MyBean implements Serializable {

	private boolean inputEnabled = false;
	private boolean modeNames = true;
	private SelectItem[] citiesNames = {
		new SelectItem("Moscow"),
		new SelectItem("Astrakhan"),
		new SelectItem("Volgograd")
	};
	private SelectItem[] citiesCodes = {
			new SelectItem("495"),
			new SelectItem("8512"),
			new SelectItem("8412")
		};
	
	private String selectedCity;
	
	public String getSelectedCity() {
		return selectedCity;
	}

	public void setSelectedCity(String selectedCity) {
		this.selectedCity = selectedCity;
	}

	public boolean isInputEnabled() {
		return inputEnabled;
	}

	public void setInputEnabled(boolean inputEnabled) {
		this.inputEnabled = inputEnabled;
	}

	public boolean isModeNames() {
		return modeNames;
	}

	public void setModeNames(boolean modeNames) {
		this.modeNames = modeNames;
	}

	public SelectItem[] getCities() {
		if (isModeNames())
			return citiesNames;
		else
			return citiesCodes;
	}

	public void toggleInput(ActionEvent event) {
		inputEnabled = !inputEnabled;
	}
	
	public void modeChanged(ValueChangeEvent event) {
		modeNames = ((Boolean)event.getNewValue()).booleanValue();
	}
	
	public String inputComplete() {
		return "";
	}
}
