package ru.edu.asu.jee.web;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ru.edu.asu.jee.dao.StudentDao;
import ru.edu.asu.jee.entity.Student;

@Named
@RequestScoped
public class StudentController {

	@Inject
	private StudentDao dao;

	private String name;
	private Integer course;
	private boolean active = true;
	
	private Integer courseFilter;
	
	private List<Student> students;
	
	public StudentDao getDao() {
		return dao;
	}

	public void setDao(StudentDao dao) {
		this.dao = dao;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCourse() {
		return course;
	}

	public void setCourse(Integer course) {
		this.course = course;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Integer getCourseFilter() {
		return courseFilter;
	}

	public void setCourseFilter(Integer courseFilter) {
		this.courseFilter = courseFilter;
	}

	public List<Student> getStudents() {
		if (courseFilter != null)
			students = dao.findByCourse(courseFilter);
		else
			students = dao.findAll();
		return students;
	}

	public String createStudent() {
		Student st = new Student(name, course, active);
		dao.persist(st);
		return "";
	}
	
	public String filterByCourse() {
		return "";
	}
	
	
}
