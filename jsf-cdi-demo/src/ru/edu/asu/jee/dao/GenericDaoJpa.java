package ru.edu.asu.jee.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

public class GenericDaoJpa<T> {
	
	private Class<T> entityClass;

	@PersistenceContext
	protected EntityManager em;

	public GenericDaoJpa(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public T find(Long id) {
		return em.find(entityClass, id);
	}

	@Transactional
	public void persist(T entity) {
		em.persist(entity);
	}

	@Transactional
	public void update(T entity) {
		em.merge(entity);
	}

	@Transactional
	public void remove(T entity) {
		em.remove(em.merge(entity));
	}

}
