package ru.edu.asu.jee.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import ru.edu.asu.jee.entity.Student;

@ApplicationScoped
public class StudentDao extends GenericDaoJpa<Student> {

	public StudentDao() {
		super(Student.class);
	}

	public List<Student> findAll() {
		return em.createNamedQuery("Student.findAll").getResultList();
	}
	
	public List<Student> findByCourse(int course) {
		return em.createNamedQuery("Student.findByCourse").setParameter("course", course).getResultList();
	}
	
}
