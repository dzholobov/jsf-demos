package ru.edu.asu.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Named;

@Named(value="demo3")
@RequestScoped
public class DemoBean3 {

	private List<Person> persons = new ArrayList<Person>();
	private DataModel<Person> personsModel = new ListDataModel<Person>(persons);
	private Person selectedPerson;

	public DemoBean3() {
		persons.add(new Person("Иван", "Иванов", 25));
		persons.add(new Person("Петр", "Сидоров", 35));
		persons.add(new Person("Андрей", "Петров", 15));
		persons.add(new Person("Ольга", "Морозова", 20));
		persons.add(new Person("Сергей", "Сергеев", 22));
	}

	public Person getSelectedPerson() {
		return selectedPerson;
	}

	public void setSelectedPerson(Person selectedPerson) {
		this.selectedPerson = selectedPerson;
	}

	public DataModel<Person> getPersonsModel() {
		return personsModel;
	}

	public void setPersonsModel(DataModel<Person> personsModel) {
		this.personsModel = personsModel;
	}

	public String selectPerson() {
		selectedPerson = personsModel.getRowData();
		return "d3result";
	}
	
}
