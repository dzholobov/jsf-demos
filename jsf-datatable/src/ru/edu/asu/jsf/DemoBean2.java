package ru.edu.asu.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.html.HtmlDataTable;
import javax.inject.Named;

@Named(value="demo2")
@RequestScoped
public class DemoBean2 {

	private List<Person> persons = new ArrayList<Person>();
	private Person selectedPerson;
	
	private HtmlDataTable personTable = new HtmlDataTable();

	public DemoBean2() {
		persons.add(new Person("Иван", "Иванов", 25));
		persons.add(new Person("Петр", "Сидоров", 35));
		persons.add(new Person("Андрей", "Петров", 15));
		persons.add(new Person("Ольга", "Морозова", 20));
		persons.add(new Person("Сергей", "Сергеев", 22));
	}

	public Person getSelectedPerson() {
		return selectedPerson;
	}

	public void setSelectedPerson(Person selectedPerson) {
		this.selectedPerson = selectedPerson;
	}

	public HtmlDataTable getPersonTable() {
		return personTable;
	}

	public void setPersonTable(HtmlDataTable personTable) {
		this.personTable = personTable;
	}

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}
	
	public String selectPerson() {
		this.selectedPerson = (Person)personTable.getRowData();
		return "";
	}
	
}
