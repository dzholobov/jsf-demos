package ru.edu.asu.jsf;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named(value="demo1")
@RequestScoped
public class DemoBean1 {

	private List<Person> persons = new ArrayList<Person>();

	public DemoBean1() {
		persons.add(new Person("Иван", "Иванов", 25));
		persons.add(new Person("Петр", "Сидоров", 35));
		persons.add(new Person("Андрей", "Петров", 15));
		persons.add(new Person("Ольга", "Морозова", 20));
		persons.add(new Person("Сергей", "Сергеев", 22));
	}
	
	public List<Person> getPersons() {
		return persons;
	}
	
	
}
